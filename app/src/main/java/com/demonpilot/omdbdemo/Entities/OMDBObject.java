package com.demonpilot.omdbdemo.Entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Demon Pilot on 11/30/2016.
 */
public class OMDBObject implements Parcelable{
    public String title;
    public String year;
    public String director;
    public String rated;
    public String runtime;
    public String genre;
    public String writer;
    public String actors;
    public String plot;
    public String language;
    public String country;
    public String awards;
    public String poster;
    public String rating;
    public String votes;
    public String id;
    public String type;

    public OMDBObject(){
        super();
    }

    public OMDBObject(String title, String year, String director, String rated, String runtime, String genre, String writer, String actors, String plot, String language, String country, String awards, String poster, String rating, String votes, String id, String type) {
        this.title = title;
        this.year = year;
        this.director = director;
        this.rated = rated;
        this.runtime = runtime;
        this.genre = genre;
        this.writer = writer;
        this.actors = actors;
        this.plot = plot;
        this.language = language;
        this.country = country;
        this.awards = awards;
        this.poster = poster;
        this.rating = rating;
        this.votes = votes;
        this.id = id;
        this.type = type;
    }

    protected OMDBObject(Parcel in) {
        title = in.readString();
        year = in.readString();
        director = in.readString();
        rated = in.readString();
        runtime = in.readString();
        genre = in.readString();
        writer = in.readString();
        actors = in.readString();
        plot = in.readString();
        language = in.readString();
        country = in.readString();
        awards = in.readString();
        poster = in.readString();
        rating = in.readString();
        votes = in.readString();
        id = in.readString();
        type = in.readString();
    }

    public static final Creator<OMDBObject> CREATOR = new Creator<OMDBObject>() {
        @Override
        public OMDBObject createFromParcel(Parcel in) {
            OMDBObject o = new OMDBObject();
            o.setTitle(in.readString());
            o.setYear(in.readString());
            o.setDirector(in.readString());
            o.setRated(in.readString());
            o.setRuntime(in.readString());
            o.setGenre(in.readString());
            o.setWriter(in.readString());
            o.setActors(in.readString());
            o.setPlot(in.readString());
            o.setLanguage(in.readString());
            o.setCountry(in.readString());
            o.setAwards(in.readString());
            o.setPoster(in.readString());
            o.setRating(in.readString());
            o.setVotes(in.readString());
            o.setId(in.readString());
            o.setType(in.readString());
            return o;
        }

        @Override
        public OMDBObject[] newArray(int size) {
            return new OMDBObject[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(year);
        dest.writeString(director);
        dest.writeString(rated);
        dest.writeString(runtime);
        dest.writeString(genre);
        dest.writeString(writer);
        dest.writeString(actors);
        dest.writeString(plot);
        dest.writeString(language);
        dest.writeString(country);
        dest.writeString(awards);
        dest.writeString(poster);
        dest.writeString(rating);
        dest.writeString(votes);
        dest.writeString(id);
        dest.writeString(type);
    }
}

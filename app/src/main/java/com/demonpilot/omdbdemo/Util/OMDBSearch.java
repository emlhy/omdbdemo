package com.demonpilot.omdbdemo.Util;

import android.util.Log;

import com.demonpilot.omdbdemo.Entities.OMDBObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

/**
 * Created by Demon Pilot on 11/30/2016.
 */
public class OMDBSearch {
    public static JSONObject searchList(String title, String page) throws IOException, JSONException {
        title = title.replaceAll("\\s+", "+");
        InputStream is = new URL("http://www.omdbapi.com/?s=" + title + "&r=json&page=" + page).openStream();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            StringBuilder sb = new StringBuilder();
            int cp;
            while ((cp = reader.read()) != -1){
                sb.append((char) cp);
            }
            String jsonText = sb.toString();
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            is.close();
        }
    }

    public static int totalResult(JSONObject jsonObject){
        if(jsonObject.has("totalResults")){
            try {
                return jsonObject.getInt("totalResults");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public static ArrayList<String> imdbID(JSONObject jsonObject){
        ArrayList<String> imdbs = new ArrayList<String>();
        JSONArray search = null;
        if(jsonObject != null && jsonObject.has("Search")){
            try {
                search = jsonObject.getJSONArray("Search");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        for(int i = 0; i < 10; i++){
            JSONObject j = null;
            try {
                j = search.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if(j != null){
                String id = null;
                try {
                    id = j.getString("imdbID");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                imdbs.add(id);
            } else{
                break;
            }
        }
        return imdbs;
    }

    public static ArrayList<OMDBObject> omdb(ArrayList<String> imdbID) throws IOException, JSONException {
        ArrayList<OMDBObject> omdbs = new ArrayList<OMDBObject>();
        for(int i = 0; i < imdbID.size(); i++){
            InputStream is = new URL("http://www.omdbapi.com/?i=" + imdbID.get(i) + "&plot=full&r=json").openStream();
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
                StringBuilder sb = new StringBuilder();
                int cp;
                while ((cp = reader.read()) != -1){
                    sb.append((char) cp);
                }
                String jsonText = sb.toString();
                JSONObject json = new JSONObject(jsonText);
                String title = json.getString("Title");
                String year = json.getString("Year");
                String director = json.getString("Director");
                String rated = json.getString("Rated");
                String runtime = json.getString("Runtime");
                String genre = json.getString("Genre");
                String writer = json.getString("Writer");
                String actors = json.getString("Actors");
                String plot = json.getString("Plot");
                String language = json.getString("Language");
                String country = json.getString("Country");
                String awards = json.getString("Awards");
                String poster = json.getString("Poster");
                String rating = json.getString("imdbRating");
                String votes = json.getString("imdbVotes");
                String id = json.getString("imdbID");
                String type = json.getString("Type");
                OMDBObject omdb = new OMDBObject(title, year, director, rated, runtime, genre, writer, actors, plot, language, country, awards, poster, rating, votes, id, type);
                //Log.d("OMDB", year+ director+ rated+ runtime+ genre+ writer+ actors+ plot+ language+ country+ awards+ poster+ rating+ votes+ id+ type+ title);
                omdbs.add(omdb);
            } finally {
                is.close();
            }
        }
        return omdbs;
    }
}

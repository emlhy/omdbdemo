package com.demonpilot.omdbdemo.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.demonpilot.omdbdemo.Entities.OMDBObject;
import com.demonpilot.omdbdemo.Fragments.DetailFragment;
import com.demonpilot.omdbdemo.Fragments.ResultListFragment;
import com.demonpilot.omdbdemo.Fragments.SearchFragment;
import com.demonpilot.omdbdemo.R;

public class MainActivity extends AppCompatActivity {
    public static final String INTENT_SEARCH = "INTENT_SEARCH";
    public static final String INTENT_NO_RESULT = "INTENT_NO_RESULT";
    public static final String INTENT_DETAIL = "INTENT_DETAIL";
    public static boolean done = false;
    public static int page = 0;

    private SearchBroadcastReceiver _searchBroadcastReceiver;
    private NoResultBroadcastReceiver _noResultBroadcastReceiver;
    private DetailBroadcastReceiver _detailBroadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(findViewById(R.id.fragment_container) != null){
            if(savedInstanceState == null){
                SearchFragment searchFragment = new SearchFragment();
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, searchFragment).commit();
            }
        }

        _searchBroadcastReceiver = new SearchBroadcastReceiver();
        _noResultBroadcastReceiver = new NoResultBroadcastReceiver();
        _detailBroadcastReceiver = new DetailBroadcastReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(_searchBroadcastReceiver);
        unregisterReceiver(_noResultBroadcastReceiver);
        unregisterReceiver(_detailBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(_searchBroadcastReceiver, new IntentFilter(INTENT_SEARCH));
        registerReceiver(_noResultBroadcastReceiver, new IntentFilter(INTENT_NO_RESULT));
        registerReceiver(_detailBroadcastReceiver, new IntentFilter(INTENT_DETAIL));
    }

    public class SearchBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            if(title != null) {
                search(title);
            }
        }
    }
    private void search(String title){
        ResultListFragment frag = new ResultListFragment();
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public class NoResultBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
             noResult();
        }
    }
    private void noResult(){
        SearchFragment frag = new SearchFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public class DetailBroadcastReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            OMDBObject o = (OMDBObject)intent.getParcelableExtra("o");
            if(o != null) {
                detail(o);
            }
        }
    }
    private void detail(OMDBObject o){
        DetailFragment frag = new DetailFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("o", o);
        frag.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment_container, frag);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}

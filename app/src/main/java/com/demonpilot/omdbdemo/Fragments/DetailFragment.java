package com.demonpilot.omdbdemo.Fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.demonpilot.omdbdemo.Activities.MainActivity;
import com.demonpilot.omdbdemo.Entities.OMDBObject;
import com.demonpilot.omdbdemo.R;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by Demon Pilot on 11/30/2016.
 */
public class DetailFragment extends Fragment{
    private OMDBObject omdbObject;
    private TextView title;
    private ImageView poster;
    private TextView genre;
    private TextView year;
    private TextView director;
    private TextView writer;
    private TextView actors;
    private TextView rated;
    private TextView runtime;
    private TextView rating;
    private TextView id;
    private TextView type;
    private TextView plot;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        MainActivity.done = true;
        omdbObject = getArguments().getParcelable("o");
        title = (TextView)rootView.findViewById(R.id.title);
        poster = (ImageView)rootView.findViewById(R.id.poster);
        genre = (TextView)rootView.findViewById(R.id.genre);
        year = (TextView)rootView.findViewById(R.id.year);
        director = (TextView)rootView.findViewById(R.id.director);
        writer = (TextView)rootView.findViewById(R.id.writer);
        actors = (TextView)rootView.findViewById(R.id.actors);
        rated = (TextView)rootView.findViewById(R.id.rated);
        runtime = (TextView)rootView.findViewById(R.id.runtime);
        rating = (TextView)rootView.findViewById(R.id.rating);
        id = (TextView)rootView.findViewById(R.id.id);
        type = (TextView)rootView.findViewById(R.id.type);
        plot = (TextView)rootView.findViewById(R.id.plot);

        title.setSelected(true);
        new AsyncTask<Void, Void, Void>() {
            Bitmap bitmap;
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    InputStream in = new URL(omdbObject.getPoster()).openStream();
                    bitmap = BitmapFactory.decodeStream(in);
                } catch (Exception e) {

                }
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
                if (bitmap != null) {
                    poster.setImageBitmap(bitmap);
                } else{
                    Bitmap icon = BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.no_picture);
                    poster.setImageBitmap(icon);
                }
            }
        }.execute();
        title.setText(omdbObject.getTitle());
        genre.setText(omdbObject.getGenre());
        year.setText(omdbObject.getYear());
        director.setText(omdbObject.getDirector());
        writer.setText(omdbObject.getWriter());
        actors.setText(omdbObject.getActors());
        rated.setText(omdbObject.getRated());
        runtime.setText(omdbObject.getRuntime());
        rating.setText(omdbObject.getRating());
        id.setText(omdbObject.getId());
        type.setText(omdbObject.getType());
        plot.setText(omdbObject.getPlot());
        return rootView;
    }
}

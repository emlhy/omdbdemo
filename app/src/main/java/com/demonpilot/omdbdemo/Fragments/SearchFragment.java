package com.demonpilot.omdbdemo.Fragments;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.demonpilot.omdbdemo.Activities.MainActivity;
import com.demonpilot.omdbdemo.R;
import com.demonpilot.omdbdemo.Util.OMDBSearch;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by Demon Pilot on 11/30/2016.
 */
public class SearchFragment extends Fragment{
    private EditText searchBar;
    private Button searchButton;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);
        MainActivity.done = false;
        MainActivity.page = 0;
        searchBar = (EditText)rootView.findViewById(R.id.search_bar);
        searchButton = (Button)rootView.findViewById(R.id.search_button);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()){
                    String title = searchBar.getText().toString();
                    if(title.equals("")){
                        Toast.makeText(getActivity(), "Title cannot be empty", Toast.LENGTH_LONG).show();
                    } else {
                        searchBar.setText("");
                        broadcastSearch(title);
                    }
                } else{
                    Toast.makeText(getActivity(), "No internet connection, please check", Toast.LENGTH_LONG).show();
                }
            }
        });
        return rootView;
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void broadcastSearch(String title){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_SEARCH);
        intent.putExtra("title", title);
        getActivity().sendBroadcast(intent);
    }
}

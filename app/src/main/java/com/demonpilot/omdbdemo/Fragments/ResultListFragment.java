package com.demonpilot.omdbdemo.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.demonpilot.omdbdemo.Activities.MainActivity;
import com.demonpilot.omdbdemo.Adapters.OMDBAdapter;
import com.demonpilot.omdbdemo.Entities.OMDBObject;
import com.demonpilot.omdbdemo.R;
import com.demonpilot.omdbdemo.Util.OMDBSearch;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Demon Pilot on 11/30/2016.
 */
public class ResultListFragment extends Fragment{
    private String title;
    private OMDBAdapter omdbAdapter;
    private ProgressDialog progressDialog;
    private int total;
    private ArrayList<String> imdbID = new ArrayList<String>();
    private ArrayList<OMDBObject> omdb = new ArrayList<OMDBObject>();
    private ArrayList<OMDBObject> finalList = new ArrayList<OMDBObject>();
    private ListView omdbList;
    private View footerView;
    private boolean loading = false;
    private Parcelable state;
    private Toast noInternet;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_result_list, container, false);
        omdbList = (ListView)rootView.findViewById(R.id.omdb_list);
        title = getArguments().getString("title");
        footerView = inflater.inflate(R.layout.footer, null);
        noInternet = Toast.makeText(getActivity(), "No internet connection, please check", Toast.LENGTH_SHORT);
        //omdbList.addFooterView(footerView);
        //footerView.setVisibility(View.GONE);
        if(!MainActivity.done) {
            MainActivity.page = 1;
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.show();
            new Search().execute(title, String.valueOf(MainActivity.page));
        } else{
            if(state != null) {
                omdbAdapter.setOMDB(finalList);
                omdbList.setAdapter(omdbAdapter);
                omdbAdapter.notifyDataSetChanged();
                omdbList.onRestoreInstanceState(state);
            }
        }
        omdbList.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem + visibleItemCount == totalItemCount && totalItemCount!=0){
                    if(isNetworkAvailable()){
                        if(!loading) {
                            noInternet.cancel();
                            loading = true;
                            MainActivity.page++;
                            omdbList.addFooterView(footerView);
                            loadMore(title, MainActivity.page, total);
                        }
                    } else{
                        noInternet.show();
                    }
                }
            }
        });
        omdbList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                OMDBObject omdbObject = finalList.get(position);
                broadcastDetail(omdbObject);
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        state = omdbList.onSaveInstanceState();
    }

    private class Search extends AsyncTask<String, Void, ArrayList<OMDBObject>> {
        JSONObject json = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<OMDBObject> doInBackground(String... params) {
            try {
                json = OMDBSearch.searchList(params[0], params[1]);
                if(json.getString("Response").equals("True")){
                    total = OMDBSearch.totalResult(json);
                    total = (int)(Math.ceil(total / 10.0));
                    imdbID = OMDBSearch.imdbID(json);
                    omdb = OMDBSearch.omdb(imdbID);
                    finalList.addAll(omdb);
                } else{
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return finalList;
        }

        @Override
        protected void onPostExecute(ArrayList<OMDBObject> o) {
            super.onPostExecute(o);
            if(o != null) {
                omdbAdapter = new OMDBAdapter(getActivity());
                omdbAdapter.setOMDB(o);
                omdbList.setAdapter(omdbAdapter);
            } else{
                Toast.makeText(getActivity(), "Movie not found", Toast.LENGTH_LONG).show();
                broadcastNoResult();
            }
            progressDialog.dismiss();
        }
    }

    private void loadMore(String title, int p, int t){
        if(p <= t){
            //footerView.setVisibility(View.VISIBLE);
            new LoadMore().execute(title, String.valueOf(p));
        } else{
            Toast.makeText(getActivity(), "No more", Toast.LENGTH_LONG).show();
            omdbList.removeFooterView(footerView);
        }
    }

    private class LoadMore extends AsyncTask<String, Void, ArrayList<OMDBObject>> {
        JSONObject json = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<OMDBObject> doInBackground(String... params) {
            try {
                json = OMDBSearch.searchList(params[0], params[1]);
                if(json.getString("Response").equals("True")){
                    imdbID = OMDBSearch.imdbID(json);
                    omdb = OMDBSearch.omdb(imdbID);
                    finalList.addAll(omdb);
                } else{
                    return null;
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return finalList;
        }

        @Override
        protected void onPostExecute(ArrayList<OMDBObject> o) {
            super.onPostExecute(o);
            omdbAdapter.notifyDataSetChanged();
            //footerView.setVisibility(View.GONE);
            loading = false;
            omdbList.removeFooterView(footerView);
        }
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void broadcastNoResult(){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_NO_RESULT);
        getActivity().sendBroadcast(intent);
    }
    private void broadcastDetail(OMDBObject o){
        Intent intent = new Intent();
        intent.setAction(MainActivity.INTENT_DETAIL);
        intent.putExtra("o", o);
        getActivity().sendBroadcast(intent);
    }
}

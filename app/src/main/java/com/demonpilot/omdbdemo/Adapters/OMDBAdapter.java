package com.demonpilot.omdbdemo.Adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.demonpilot.omdbdemo.Entities.OMDBObject;
import com.demonpilot.omdbdemo.R;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Demon Pilot on 12/2/2016.
 */
public class OMDBAdapter extends ArrayAdapter<OMDBObject>{
    private ArrayList<OMDBObject> omdb;
    private Context context;
    public OMDBAdapter(Context context) {
        super(context, R.layout.item_omdb);
        this.context = context;
    }

    @Override
    public int getCount() {
        return omdb.size();
    }

    @Override
    public OMDBObject getItem(int position) {
        return omdb.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView == null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_omdb, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.thumb = (ImageView) convertView.findViewById(R.id.thumb);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.title.setSelected(true);
            viewHolder.director = (TextView) convertView.findViewById(R.id.director);
            viewHolder.director.setSelected(true);
            viewHolder.year = (TextView) convertView.findViewById(R.id.year);
            convertView.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        final OMDBObject omdbObject = getItem(position);
        new AsyncTask<Void, Void, Void>() {
            Bitmap bitmap;
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    InputStream in = new URL(omdbObject.getPoster()).openStream();
                    bitmap = BitmapFactory.decodeStream(in);
                } catch (Exception e) {

                }
                return null;
            }
            @Override
            protected void onPostExecute(Void result) {
                if (bitmap != null) {
                    viewHolder.thumb.setImageBitmap(bitmap);
                } else{
                    //Bitmap.Config conf = Bitmap.Config.ARGB_8888;
                    //Bitmap bmp = Bitmap.createBitmap(500, 500, conf);
                    Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.no_picture);
                    viewHolder.thumb.setImageBitmap(icon);
                }
            }
        }.execute();
        viewHolder.title.setText(omdbObject.getTitle());
        viewHolder.director.setText(omdbObject.getDirector());
        viewHolder.year.setText(omdbObject.getYear());

        return convertView;
    }

    public void setOMDB(ArrayList<OMDBObject> omdb){
        this.omdb = omdb;
    }

    private static class ViewHolder{
        ImageView thumb;
        TextView title;
        TextView director;
        TextView year;
    }
}
